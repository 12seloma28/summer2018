#ifndef BINARYTREE_H
#define BINARYTREE_H
#include <string>


/**
 * @file BinaryTree.h
 * @author Lukas Majoros
 * @date 20 Dubna 2018
 * @brief Hlavickovy soubor tridy BinaryTree
 *
 * Hlavickovy soubor tridy BinaryTree,
 * obsahuje strukturu Node
 * a funkce Insert, Clear, Serialize a deserialize
 */

/****************************************************************************
 *   Trida BinaryTree je zakladem stejna jak ze cviceni,                    *
 *   pouze jsou implementovany metody Print, serializace a deserializace    *
 ****************************************************************************/

using namespace std;



struct Node {
	int Key;
	Node *Left, *Right;
};
/**
 * @brief Struktura Node
 *
 * @param Key - Klic / koren
 * @param *Left - Ukazatel na leveho potomka
 * @param *Right - Ukazatel na praveho potomka
 */

/**
 * @brief Trida BinaryTree
 *
 * private:
 * @param Node * root - Koren stromu.
 * @param Insert() - Funkce pro vkladani cisel do stromu
 * @param Clear() - Funkce, ktera maze koren / ukazatele v levo/pravo.
 *
 *
 * public:
 * @param BinaryTree() - konstruktor
 * @param ~BinaryTree() - destruktor
 * @param Insert() - Funkce pro vkladani cisel do stromu
 * @param Clear() - Funkce, ktera smaze cely strom.
 * @param Print() - Funkce pro vypis stromu
 * @param Serialize() - Funkce pro serializaci stromu
 * @param Deserialize() - Funkce pro deserializaci stromu
 */
class BinaryTree {
private:
	Node * root;

	void Insert(Node *& node, const int &value);
	void Clear(Node *& node);
public:
	BinaryTree();
	~BinaryTree();

	void Insert(const int &value);
	void Clear();

	void Print();
	string Serialize();
	static BinaryTree * Deserialize(string data);
};

#endif // BINARYTREE_H
